import {Routes, RouterModule, Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {PaginaOneComponent} from "./components/pagina-one/pagina-one.component";
import {NgModule} from "@angular/core";
import {PaginaTwoComponent} from "./components/pagina-two/pagina-two.component";
import {PaginaThreeComponent} from "./components/pagina-three/pagina-three.component";
import {VideoPresentacionComponent} from "./components/video-presentacion/video-presentacion.component";


export const routes: Routes = [
  {
    path: 'page-one',
    component: PaginaOneComponent,
  },
  {
    path: 'page-two',
    component: PaginaTwoComponent,

  },
  {
    path: 'page-three',
    component: PaginaThreeComponent,
    children: [
      {
        path: 'video-presentacion', component: VideoPresentacionComponent
      },
      {
        path: 'presentacion', component: PaginaTwoComponent
      }
    ]
  }
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}


