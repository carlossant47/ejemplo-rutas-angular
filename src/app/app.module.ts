import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PaginaOneComponent } from './components/pagina-one/pagina-one.component';
import { PaginaTwoComponent } from './components/pagina-two/pagina-two.component';
import { PaginaThreeComponent } from './components/pagina-three/pagina-three.component';
import { AppRoutingModule } from './app-routing.module';
import { VideoPresentacionComponent } from './components/video-presentacion/video-presentacion.component';
@NgModule({
  declarations: [
    AppComponent,
    PaginaOneComponent,
    PaginaTwoComponent,
    PaginaThreeComponent,
    VideoPresentacionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
