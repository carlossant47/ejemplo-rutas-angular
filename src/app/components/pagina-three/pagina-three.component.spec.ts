import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaThreeComponent } from './pagina-three.component';

describe('PaginaThreeComponent', () => {
  let component: PaginaThreeComponent;
  let fixture: ComponentFixture<PaginaThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginaThreeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaginaThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
