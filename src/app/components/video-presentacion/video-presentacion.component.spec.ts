import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoPresentacionComponent } from './video-presentacion.component';

describe('VideoPresentacionComponent', () => {
  let component: VideoPresentacionComponent;
  let fixture: ComponentFixture<VideoPresentacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoPresentacionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VideoPresentacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
